import { getContainer, logError, manifest, prefixName, searchParameterName } from './common.js'

/**
 * menu commands map.
 * @type {{apply: string}}
 */
const menu = {
	grouped: prefixName('group'),
	apply: prefixName('apply', 'bookmark'),
	none: prefixName('none')
};

/**
 * top level menu.
 */
browser.menus.create({
	id: menu.grouped,
	title: manifest.name.toLowerCase(),
	contexts: ["bookmark"]
});

/**
 * 2nd level apply menu.
 */
browser.menus.create({
	id: menu.apply,
	title: 'apply container',
	parentId: menu.grouped,
	contexts: ['bookmark'],
}, async () => {
	await browser.menus.create({
		id: menu.none,
		title: 'none',
		contexts: ['bookmark'],
		parentId: menu.apply,
	});

	(await browser.contextualIdentities.query({})).forEach(container => browser.menus.create({
		id: container.name,
		title: container.name,
		icons: {
			16: `${container.iconUrl}#${container.color}`
		},
		contexts: ['bookmark'],
		parentId: menu.apply,
	}))
})


/**
 * checks if container and bookmark ids are valid and returns container and bookmark objects.
 * @param {string} bookmarkId
 * @param {string} containerName
 */
async function getContainerAndBookmarkFromIds(bookmarkId, containerName) {
	const [container, bookmarks] = await Promise.all([
		containerName === 'none' ? 'none' : getContainer(containerName),
		browser.bookmarks.get(bookmarkId)])

	return {container, bookmark: bookmarks[0]};
}


/**
 * @param {browser.contextualIdentities.ContextualIdentity} container
 * @param {browser.bookmarks.BookmarkTreeNode} bookmark
 */
async function updateBookmarkContainer(bookmark, container) {
	switch (bookmark.type) {
		case "folder":
			bookmark.children.forEach(childBookmark => updateBookmarkContainer(childBookmark, container));
			break;
		case "bookmark":
			const url = new URL(bookmark.url);
			url.searchParams.set(searchParameterName, container.name);
			await browser.bookmarks.update(bookmark.id, {
				url: url.toString()
			}).catch(logError);
			break;
	}
}

browser.menus.onClicked.addListener(async (info) => {
	if (info.parentMenuItemId !== menu.apply) return;
	const {container, bookmark} = await getContainerAndBookmarkFromIds(info.bookmarkId, info.menuItemId);
	await updateBookmarkContainer(bookmark, container);
})
