export const manifest = browser.runtime.getManifest();
export const searchParameterName = manifest.short_name;

export class ContainerNotFoundError extends Error {
	constructor(message = "container not found") {
		super(message);
		this.name = "ContainerNotFoundError"
	}
}

/**
 * gets the first container with name else throws an error.
 * @param name {string}
 */
export const getContainer = async (name) => {
	const containers = await browser.contextualIdentities.query({name})
	if (!containers.length) throw new ContainerNotFoundError();
	return containers[0]
}

/**
 * simple logging.
 * @param error {Error}
 */
export function logError(error) {
	console.error(error);
}

/**
 * Prefixes name of extension.
 * @param names {string} Name parts.
 * @returns {string}
 */
export function prefixName(...names) {
	return [searchParameterName, ...names].join('-')
}
