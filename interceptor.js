import { getContainer, logError, searchParameterName } from "./common.js";

const processing = new Set();

/**
 * @param tab {browser.tabs.Tab}
 */
async function interceptTab(tab) {
	const url = new URL(tab.url);
	if (!url.searchParams.has(searchParameterName)) return;
	const containerName = url.searchParams.get(searchParameterName);
	url.searchParams.delete(searchParameterName);
	if (processing.has(tab.id)) return;
	processing.add(tab.id);

	await getContainer(containerName)
		.then(async container => {
			if (tab.cookieStoreId === container.cookieStoreId) {
				return await browser.tabs.update({url: url.toString()}).catch(logError);
			}

			await browser.tabs.create({
				url: url.toString(),
				cookieStoreId: container.cookieStoreId,
				index: tab.index + 1,
			});
			await browser.tabs.remove(tab.id);
		})
		.catch(logError);
	/**
	 * 1 sec or else, unwanted looping.
	 */
	setTimeout(() => processing.delete(tab.id), 1000);
}

browser.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
	if (["about:newtab", "about:blank"].includes(tab.url)) return;
	if (changeInfo.status !== 'loading') return;
	interceptTab(tab);
});
